package gpa;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.TreeMap;
import java.util.TreeSet;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;


public class GPA {
    class Student {

        //名字
        String name;
        //所属班级
        String classes;
        //学号
        String studentid;
        //挂科次数
        int failcount;
        //挂科学分数
        int failpoint;
        //必修+限选学分数
        double obligatoryscore;
        //必修+限选总分数
        double obligatorypoint;
        //任选学分数
        double draftscore;
        //任选学分点数
        double draftpoint;

        Student() {
            obligatoryscore = 0.0;
            obligatorypoint = 0.0;
            draftscore = 0.0;
            draftpoint = 0.0;
            name = "";
            classes = "";
            studentid = "";
            failcount = 0;
            failpoint = 0;
        }

        public void addobligatorypoint(double score, double point) {
            this.obligatorypoint += point;
            this.obligatoryscore += score;
        }

        public void adddraft(double score, double point) {
            this.draftpoint += point;
            this.draftscore += score;
        }

        public void setname(String name) {
            if (name == null) return;
            else this.name = name;
        }

        public void setstudentid(String id) {
            if (id == null) return;
            else this.studentid = id;
        }

        public void setclasses(String classes) {
            if (classes == null) return;
            else this.classes = classes;
        }

        public double getObligatoryGPA() {
            if (this.obligatorypoint > 0.0)
                return this.obligatoryscore / this.obligatorypoint;
            else return 0.0;
        }

        public double getTotalGPA() {
            if (this.obligatorypoint + this.draftpoint > 0.0)
                return (this.obligatoryscore + this.draftscore) / (this.obligatorypoint + this.draftpoint);
            else return 0.0;
        }

        //得到总的分数点
        public double getTotalScore() {
            return this.obligatoryscore + this.draftscore;
        }

        public String getclasses() {
            if (classes == null) return "";
            else return classes;
        }

        public String getstudentid() {
            if (studentid == null) return "";
            else return studentid;
        }

        public String getname() {
            if (name == null) return "";
            else return name;
        }

        public int getfailcount() {
            return this.failcount;
        }

        public int getfailpoint() {
            return this.failpoint;
        }
    }


    /**
     * @param input 输入文件
     * @param output 输出文件
     * @param isPersentage 是否是新规则
     */
    void computeGPA(String input, String output, boolean isPersentage) {
        try {
            HSSFWorkbook hssfworkbook = new HSSFWorkbook(new FileInputStream(input));
            HSSFSheet hssfsheet = hssfworkbook.getSheetAt(0);

            //读入从info上面导出得excel数据表，只会处理第一个表
            System.out.println((hssfsheet.getLastRowNum() - hssfsheet.getFirstRowNum() - 1) + " records on sheet \"" + hssfworkbook.getSheetName(0) + "\" and we only use the first sheet");
            HSSFRow hssfrow = hssfsheet.getRow(hssfsheet.getFirstRowNum());
            int studentid = 0;
            int courseid = 0;
            int classes = 0;
            int name = 0;
            int score = 0;
            int point = 0;
            int type = 0;
            HSSFCell hssfcell;
            //得到列的属性
            // 这里j记录的是列的编号
            for (int j = 0; j < hssfrow.getLastCellNum(); j++) {
                hssfcell = hssfrow.getCell(j);
                if (hssfcell.toString().contains("学号")) {
                    studentid = j;
                } else if (hssfcell.toString().contains("姓名")) {
                    name = j;
                } else if (hssfcell.toString().contains("教学班级")) {
                    classes = j;
                } else if (hssfcell.toString().contains("绩点成绩")) {
                    continue;
                } else if (hssfcell.toString().contains("成绩")) {
                    score = j;
                } else if (hssfcell.toString().contains("学分")) {
                    point = j;
                } else if (hssfcell.toString().contains("课程属性")) {
                    type = j;
                }
            }
            TreeMap<String, Student> map = new TreeMap<String, Student>();
            Student student;
            for (int i = hssfsheet.getFirstRowNum() + 1; i <= hssfsheet.getLastRowNum(); i++) {
                hssfrow = hssfsheet.getRow(i);
                hssfcell = hssfrow.getCell(studentid);
                if (!map.containsKey(hssfcell.toString())) {
                    student = new Student();
                    map.put(hssfcell.toString(), student);
                }
                student = map.get(hssfcell.toString());
                student.setstudentid(hssfcell.toString());
                student.setclasses(hssfrow.getCell(classes).toString());
                student.setname(hssfrow.getCell(name).toString());

                double scores = 0.0;
                //这是学分

                double points_db = Double.parseDouble(hssfrow.getCell(point).toString());
                int points = (int) points_db;

                if (hssfrow.getCell(score).toString().contains("不通过")) {
                    student.failcount++;
                    student.failpoint += points;
                    continue;
                } else if (hssfrow.getCell(score).toString().contains("通过")
                        || hssfrow.getCell(score).toString().contains("优秀")
                        || hssfrow.getCell(score).toString().contains("缓考")
                        || hssfrow.getCell(score).toString().contains("免课")
                        || hssfrow.getCell(score).toString().contains("W")
                        || hssfrow.getCell(score).toString().contains("I")
                        || hssfrow.getCell(score).toString().contains("N/A")
                        || hssfrow.getCell(score).toString().contains("免修")) {
                    continue;
                } else {
                    // System.out.println(hssfrow.getCell(score));

                    scores = Double.parseDouble(hssfrow.getCell(score).toString());

                    if ( isPersentage && scores < 60.0) {
                        student.failcount++;
                        student.failpoint += points;
                    }
                    else if (!isPersentage && scores < 1.0)
                    {
                        student.failcount++;
                        student.failpoint += points;
                    }
                }
                if (hssfrow.getCell(type).toString().contains("任选")) {
                    student.adddraft(scores * points, points);
                } else if (hssfrow.getCell(type).toString().contains("必修") || hssfrow.getCell(type).toString().contains("限选")) {
                    student.addobligatorypoint(scores * points, points);
                } else {
                    System.out.println(hssfrow.getCell(type).toString());
                }
            }

            hssfworkbook = new HSSFWorkbook();
            //输出个人成绩表格
            hssfsheet = hssfworkbook.createSheet("个人学分绩");

            //第0行是title
            hssfrow = hssfsheet.createRow(0);
            hssfrow.createCell(0).setCellValue("班级");
            hssfrow.createCell(1).setCellValue("学号");
            hssfrow.createCell(2).setCellValue("姓名");
            hssfrow.createCell(3).setCellValue("必修+限选");
            hssfrow.createCell(4).setCellValue("必修+限选+选修");
            hssfrow.createCell(5).setCellValue("总成绩点");
            hssfrow.createCell(6).setCellValue("不及格门数");
            hssfrow.createCell(7).setCellValue("不及格学分");
            //数据都从第一行开始读写
            int currentrow = 1;
            for (String s : map.keySet()) {
                student = map.get(s);
                hssfrow = hssfsheet.createRow(currentrow++);
                hssfrow.createCell(0).setCellValue(student.getclasses());
                hssfrow.createCell(1).setCellValue(student.getstudentid());
                hssfrow.createCell(2).setCellValue(student.getname());
                hssfrow.createCell(3).setCellValue(student.getObligatoryGPA());
                hssfrow.createCell(4).setCellValue(student.getTotalGPA());
                hssfrow.createCell(5).setCellValue(student.getTotalScore());
                if (student.getfailcount() > 0.0) hssfrow.createCell(6).setCellValue(student.getfailcount());
                if (student.getfailpoint() > 0.0) hssfrow.createCell(7).setCellValue(student.getfailpoint());
            }
            //输出学分积计算细节
            hssfsheet = hssfworkbook.createSheet("计算细节");

            hssfrow = hssfsheet.createRow(0);
            hssfrow.createCell(0).setCellValue("班级");
            hssfrow.createCell(1).setCellValue("学号");
            hssfrow.createCell(2).setCellValue("姓名");
            hssfrow.createCell(3).setCellValue("必修+限选成绩点");
            hssfrow.createCell(4).setCellValue("必修+限选学分");
            hssfrow.createCell(5).setCellValue("总成绩点");
            hssfrow.createCell(6).setCellValue("总学分");
            hssfrow.createCell(7).setCellValue("不及格门数");
            hssfrow.createCell(8).setCellValue("不及格学分");
            currentrow = 1;
            for (String s : map.keySet()) {
                student = map.get(s);
                hssfrow = hssfsheet.createRow(currentrow++);
                hssfrow.createCell(0).setCellValue(student.getclasses());
                hssfrow.createCell(1).setCellValue(student.getstudentid());
                hssfrow.createCell(2).setCellValue(student.getname());
                hssfrow.createCell(3).setCellValue(student.obligatoryscore);
                hssfrow.createCell(4).setCellValue(student.obligatorypoint);
                hssfrow.createCell(5).setCellValue(student.getTotalScore());
                hssfrow.createCell(6).setCellValue(student.obligatorypoint + student.draftpoint);
                if (student.getfailcount() > 0.0) hssfrow.createCell(7).setCellValue(student.getfailcount());
                if (student.getfailpoint() > 0.0) hssfrow.createCell(8).setCellValue(student.getfailpoint());
            }

            //输出到excel
            FileOutputStream fileoutputstream = new FileOutputStream(output);
            hssfworkbook.write(fileoutputstream);
            fileoutputstream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {


        if (args.length != 2) {
            System.out.println("usage: java -jar GPA.jar inputfile outputfile");
        }
        new GPA().computeGPA(args[0], args[1],true);

    }

}
